#!/usr/bin/env bash

CONFIGURATION="${CONFIGURATION:-/etc/smtprelay/config.ini}"

exec smtprelay -config "$CONFIGURATION"
