FROM registry.gitlab.com/mangadex-pub/debuilder/bullseye:main as builder

ENV PATH "$PATH:/usr/local/go/bin"

ARG GOLANG_VERSION
RUN curl -sfSL -o golang.tar.gz "https://go.dev/dl/go${GOLANG_VERSION}.linux-amd64.tar.gz" && \
    tar -C /usr/local -xzf golang.tar.gz && \
    rm -v golang.tar.gz && \
    go version

WORKDIR /src

ARG SMTPRELAY_VERSION
RUN curl -sfSL -o smtprelay.tar.gz "https://github.com/decke/smtprelay/archive/refs/tags/v${SMTPRELAY_VERSION}.tar.gz" && \
    mkdir -pv smtprelay && \
    tar -C "smtprelay" --strip-components=1 -xf smtprelay.tar.gz

WORKDIR /src/smtprelay

ARG IMAGE_VERSION="local-SNAPSHOT"
RUN export IMAGE_BUILD_DATE="$(date -u -I)" && \
    export LDFLAGS="-s -w -X main.appVersion=${IMAGE_VERSION} -X main.buildTime=$IMAGE_BUILD_DATE" && \
    go build -ldflags "$LDFLAGS"

ARG DEBIAN_CODENAME
FROM docker.io/library/debian:${DEBIAN_CODENAME}-slim

RUN apt -qq update && \
    apt -qq -y --no-install-recommends install \
      apt-utils \
      apt-transport-https \
      ca-certificates && \
    sed -i -e 's/http\:/https\:/g' /etc/apt/sources.list && \
    apt -qq update && \
    apt -qq -y --no-install-recommends install \
      ca-certificates \
      curl \
      debian-archive-keyring && \
    apt -qq -y --purge autoremove && \
    apt -qq -y clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/* /var/log/*

COPY --from=builder --chown=root:root /src/smtprelay/smtprelay /usr/local/bin/smtprelay

RUN groupadd -r -g 999 mangadex && useradd --system -u 999 -r -g 999 mangadex
USER mangadex
WORKDIR /tmp

RUN smtprelay -version

COPY --chown=root:root docker-entrypoint.sh /docker-entrypoint.sh
CMD ["/docker-entrypoint.sh"]
